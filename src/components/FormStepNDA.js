import React, { useEffect, useRef, useState } from 'react'
import ReactSignatureCanvas from 'react-signature-canvas'

export const FormStepNDA = ({ meeting, updateMeeting, advanceFormStep }) => {
  const signatureCanvas = useRef(null)
  const [ndaText, setNdaText] = useState('')

  const personalizedNdaText = ndaText.replace(/{NAME}/, meeting.visitor)

  useEffect(() => {
    const doit = async () => {
      const response = await fetch('https://nmuth-darkinterview.builtwithdark.com/nda')
      const { data } = await response.json()
      setNdaText(data.text)
    }

    doit()
  }, [])

  const onDone = evt => {
    evt.preventDefault()

    if (signatureCanvas.current.isEmpty()) {
      alert('Please sign the NDA to continue.')
      return
    }

    updateMeeting({ ndaSignature: signatureCanvas.current.toDataURL('image/png') })
    advanceFormStep()
  }

  const onClickReset = evt => {
    evt.preventDefault()
    evt.stopPropagation()

    signatureCanvas.current.clear()
  }

  return (
    <>
      <h1>legal stuff</h1>
      <p>{personalizedNdaText}</p>
      <form onSubmit={onDone}>
        <div>
          <ReactSignatureCanvas
            ref={ref => signatureCanvas.current = ref}
            canvasProps={{width: 512, height: 128, className: 'FormStepNDA__Signature'}}/>
        </div>
        <div className="FormStepNDA__Buttons">
          <button type="button" onClick={onClickReset}>reset</button>
          <button type="submit">I agree</button>
        </div>
      </form>
    </>
  )
}
