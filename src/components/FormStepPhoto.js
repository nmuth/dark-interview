import React, { useState } from 'react'
import { useWebcam } from '../hooks'
import { LiveVideoFeed } from './LiveVideoFeed'

export const FormStepPhoto = ({ updateMeeting, advanceFormStep }) => {
  const [videoEl, setVideoEl] = useState(null)
  const video = useWebcam()

  const onTakePhoto = () => {
    const aspectRatio = video.settings.width / video.settings.height

    const canvas = document.createElement('canvas')
    canvas.style = {
      position: 'absolute',
      left: -1000,
      top: -1000,
      width: videoEl.videoWidth,
      height: videoEl.videoHeight,
    }
    document.body.appendChild(canvas)
    const ctx = canvas.getContext('2d')
    ctx.drawImage(videoEl, 0, 0, canvas.width, canvas.height)

    const photo = canvas.toDataURL('image/png')

    document.body.removeChild(canvas)
    video.stream.getTracks()[0].stop()

    updateMeeting({ photo })
    advanceFormStep()
  }

  return (
    <>
      <h1>smile!</h1>
      <LiveVideoFeed onVideoChange={setVideoEl} stream={video.stream}/>
      <button onClick={onTakePhoto}>take photo</button>
    </>
  )
}
