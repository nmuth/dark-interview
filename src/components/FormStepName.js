import React, { useState } from 'react'

export const FormStepName = ({ updateMeeting, advanceFormStep }) => {
  const [name, setName] = useState('')

  const onDone = evt => {
    evt.preventDefault()
    updateMeeting({ visitor: name })
    advanceFormStep()
  }

  return (
    <>
      <h1>hi.</h1>
      <form className="FormStepName" onSubmit={onDone}>
        <input
          type="text"
          value={name}
          placeholder="Enter your name"
          onChange={evt => setName(evt.target.value)}/>
        <button type="submit">go</button>
      </form>
    </>
  )
}
