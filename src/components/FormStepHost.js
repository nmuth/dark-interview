import React, { useEffect, useState } from 'react'

export const FormStepHost = ({ updateMeeting, advanceFormStep }) => {
  const [availableHosts, setAvailableHosts] = useState([])

  useEffect(() => {
    const doit = async () => {
      const resp = await fetch('https://nmuth-darkinterview.builtwithdark.com/hosts')

      const { data } = await resp.json()
      setAvailableHosts(data)
    }

    doit()
  }, [])

  const onSelectHost = host => {
    updateMeeting({ host })
    advanceFormStep()
  }

  const items = availableHosts.map((host, idx) => (
    <li className="FormStepHost__HostItem" key={idx} onClick={() => onSelectHost(host)}>
      {host.name}
    </li>
  ))

  return (
    <>
      <h1>who are you here to see?</h1>
      <ul className="FormStepHost__HostsList">
        {items}
      </ul>
    </>
  )
}
