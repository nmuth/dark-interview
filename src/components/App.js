import React, { useState } from 'react'
import './style.css'
import { Meeting } from '../models/meeting'
import { Container } from './Container'
import { FormStepName } from './FormStepName'
import { FormStepHost } from './FormStepHost'
import { FormStepNDA } from './FormStepNDA'
import { FormStepPhoto } from './FormStepPhoto'
import { FormStepConfirm } from './FormStepSuccess'

const FORM_STEPS = {
  Name: 'Name',
  Host: 'Host',
  NDA: 'NDA',
  TakePhoto: 'TakePhoto',
  Success: 'Success',
}

const FORM_STEPS_IN_ORDER = [
  FORM_STEPS.Name,
  FORM_STEPS.Host,
  FORM_STEPS.NDA,
  FORM_STEPS.TakePhoto,
  FORM_STEPS.Success,
]

const formStepAdvancer = (currentStep, setFormStep) => {
  const currentIndex = FORM_STEPS_IN_ORDER.indexOf(currentStep)
  const nextStep = FORM_STEPS_IN_ORDER[currentIndex + 1]

  if (!nextStep) {
    return () => {}
  }

  return () => setFormStep(nextStep)
}

function App() {
  const [formStep, setFormStep] = useState(FORM_STEPS.Name)
  const [meeting, setMeeting] = useState(Meeting())

  const updateMeeting = data => setMeeting({ ...meeting, ...data })

  return (
    <Container>
      <CurrentStep
        step={formStep}
        setStep={setFormStep}
        meeting={meeting}
        updateMeeting={updateMeeting}/>
    </Container>
  )
}

const CurrentStep = ({ step = FORM_STEPS.Name, meeting, updateMeeting, setStep }) => {
  const advanceFormStep = formStepAdvancer(step, setStep)

  switch (step) {
    case FORM_STEPS.Name:
      return (
        <FormStepName
          updateMeeting={updateMeeting}
          advanceFormStep={advanceFormStep}/>
      )
    case FORM_STEPS.Host:
      return (
        <FormStepHost
          updateMeeting={updateMeeting}
          advanceFormStep={advanceFormStep}/>
      )
    case FORM_STEPS.NDA:
      return (
        <FormStepNDA
          meeting={meeting}
          updateMeeting={updateMeeting}
          advanceFormStep={advanceFormStep}/>
      )
    case FORM_STEPS.TakePhoto:
      return (
        <FormStepPhoto
          updateMeeting={updateMeeting}
          advanceFormStep={advanceFormStep}/>
      )
    case FORM_STEPS.Success:
      return (
        <FormStepConfirm
          meeting={meeting}
          updateMeeting={updateMeeting}
          advanceFormStep={advanceFormStep}/>
      )
    default:
      throw new Error(`step ${step} does not exist!!`)
  }
}

export default App
