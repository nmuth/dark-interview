import React, { useRef } from 'react'

/**
 * Offscreen canvas that takes in a <video/> element and captures a frame.
 */
export const VideoFrameCapturer = React.memo(({ videoEl = null, onCaptureFrame = () => {} }) => {
  const canvasRef = useRef(null)
  const canvas = canvasRef.current
  const ctx = canvas && canvas.getContext('2d')

  useInterval(() => {
    if (!ctx || !videoEl) {
      return
    }

    ctx.drawImage(videoEl, 0, 0, videoEl.width, videoEl.height)
    onCaptureFrame(canvas.toDataURL('image/png'))
  }, interval)

  return (
    <div>
      <canvas ref={canvasRef} style={{ position: 'absolute', left: -1000, top: -1000, width: 128, height: 128 }}/>
    </div>
  )
})
