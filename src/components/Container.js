import React from 'react'

export const Container = ({ children }) => (
  <main>
    {children}
  </main>
)
