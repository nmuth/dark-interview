import React, { useCallback, useState } from 'react'

export const FormStepConfirm = ({ meeting }) => {
  const [submitStatus, setSubmitStatus] = useState({ loading: false, error: null, result: null })

  const onSubmit = useCallback(
    async () => {
      setSubmitStatus({ loading: true, error: null, result: null })

      const requestBody = {
        visitor: meeting.visitor,
        host: meeting.host.name,
        signatureBase64: meeting.ndaSignature,
        photoBase64: meeting.photo,
      }

      const response = await fetch('https://nmuth-darkinterview.builtwithdark.com/meeting', {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
        },
        body: JSON.stringify(requestBody)
      })

      if (response.status >= 400) {
        setSubmitStatus({
          ...submitStatus,
          loading: false,
          error: 'Something went wrong communicating with the server :('
        })
        return
      }

      setSubmitStatus({
        loading: false,
        error: null,
        result: await response.json()
      })
    },
    [meeting, submitStatus, setSubmitStatus]
  )

  return (
    <>
      <h1>you're all set</h1>
      <p>sit tight—someone will be with you soon!</p>
      <Badge meeting={meeting}/>
      <button onClick={onSubmit}>ready!</button>
    </>
  )
}

const Badge = ({ meeting }) => (
  <div className="Badge">
    <div className="Badge__AvatarContainer">
      <img className="Badge__Avatar"
           src={meeting.photo}/>
    </div>
    <div className="Badge__Info">
      <strong className="Badge__InfoKey">Name:</strong>
      <span className="Badge__InfoValue">{meeting.visitor}</span>
      <strong className="Badge__InfoKey">Here to see:</strong>
      <span className="Badge__InfoValue">{meeting.host.name}</span>
    </div>
  </div>
)
