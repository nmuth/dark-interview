export const Meeting = (data = {}) => ({
  visitor: '',
  host: null,
  ndaSignature: null,
  photo: null,
  ...data,
})
