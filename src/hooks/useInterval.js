import { useRef, useEffect } from 'react'

/**
 * Run the provided callback function on an interval.
 *
 * slightly modified from Dan Abramov's blog post: https://overreacted.io/making-setinterval-declarative-with-react-hooks/
 * Major thanks to Dan for making the usefulness of hooks finally click for me!
 */
export const useInterval = (fn, delay) => {
  const savedCallback = useRef()

  useEffect(() => {
    savedCallback.current = fn
  })

  useEffect(() => {
    if (!delay) {
      return
    }

    const interval = setInterval(() => savedCallback.current && savedCallback.current(), delay)

    return () => clearInterval(interval)
  }, [delay])
}
